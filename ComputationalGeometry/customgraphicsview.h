#pragma once

#include <QGraphicsView>
#include <QMouseEvent>

class CustomGraphicsView : public QGraphicsView
{
    Q_OBJECT;

public:
    CustomGraphicsView(QWidget* parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent* event);

signals:
    void pointCreated(const QPointF& point);

    void closePolygon();
};

