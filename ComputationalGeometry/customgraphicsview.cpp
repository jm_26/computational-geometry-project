#include "customgraphicsview.h"

CustomGraphicsView::CustomGraphicsView(QWidget* parent)
    : QGraphicsView(parent)
{}

void CustomGraphicsView::mousePressEvent(QMouseEvent* event)
{
    // Add points with left-click
    if (event->button() == Qt::LeftButton)
    {
        QPointF scenePoint = mapToScene(event->pos());

        emit pointCreated(scenePoint);
    }

    // Close the polygon in triangulation algorithm with right-click
    if (event->button() == Qt::RightButton)
    {
        emit closePolygon();
    }

    QGraphicsView::mousePressEvent(event);
}
